<?php

function get_ip_address() {
  $ipaddress = '';
  if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
  }
  elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
  }
  elseif (!empty($_SERVER['HTTP_X_FORWARDED'])) {
    $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
  }
  elseif (!empty($_SERVER['HTTP_FORWARDED_FOR'])) {
    $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
  }
  elseif (!empty($_SERVER['HTTP_FORWARDED'])) {
    $ipaddress = $_SERVER['HTTP_FORWARDED'];
  }
  elseif (!empty($_SERVER['REMOTE_ADDR'])) {
    $ipaddress = $_SERVER['REMOTE_ADDR'];
  }
  else {
    $ipaddress = 'UNKNOWN';
  }
  if (validate_ip($ipaddress)) {
    return $ipaddress;
  }
  else {
    return FALSE;
  }
}

function validate_ip($ip) {
  if (strtolower($ip) === 'unknown') {
    return FALSE;
  }
  // generate ipv4 network address
  $long = ip2long($ip);
  ;
  if ($long == -1 || $long === FALSE) {
    return FALSE;
  }
  else {
    return TRUE;
  }
}

function _havan_sso_user_scan($uid = NULL) {
  $domain = $_SERVER['HTTP_HOST'];
  if (_havan_sso_check_user_exist($uid, NULL, FALSE)) {
    // Start session for this user
    if (!_havan_sso_check_user_exist($uid, $domain, FALSE)) {
      _havan_sso_check_session_save($uid);
    }
  }
}

function _havan_sso_check_user_exist($uid = NULL, $domain = NULL, $list = FALSE) {
  global $havan_sid;
  $client_ip = get_ip_address();
  if ($client_ip) {
    $query_havan_user = db_select('havan_login_sessions', 'hls');
    $query_havan_user->fields('hls', array('havan_sid'));
    if ($uid) {
      $query_havan_user->condition('hls.havan_uid', $uid, '=');
    }
    if ($havan_sid) {
      $query_havan_user->condition('hls.havan_sid', $havan_sid, '=');
    }
    if ($domain) {
      $query_havan_user->condition('hls.havan_origin', $domain, '=');
    }
    $query_havan_user->condition('hls.havan_client_ip', $client_ip, '=');
    if ($list) {
      return $query_havan_user->execute()->fetchAll();
    }
    else {
      return $query_havan_user->execute()->rowCount();
    }
  }
  return FALSE;
}

function _havan_sso_check_session_save($uid = NULL, $session_id = NULL) {
  global $user;
  global $havan_sid;
  $client_ip = get_ip_address();

  if ($client_ip) {
    $domain = $_SERVER['HTTP_HOST'];
    // Start session onn this Domain.
    if (!$session_id) {
      $user = user_load($uid);
      drupal_session_regenerate();
      $session_id = session_id();
      $havan_sid = $session_id;
    }
    $query_havan_sso = db_insert('havan_login_sessions')
            ->fields(array('havan_uid' => $uid, 'havan_client_ip' => $client_ip, 'havan_sid' => $session_id, 'havan_origin' => $domain, 'havan_login_stamp' => time()))->execute();
    $site_frontpage = variable_get('site_frontpage', 'node');

    // To handle conditions
    if ($user->access != 0) {
      if (arg(1) != 'reset') {
        drupal_goto($site_frontpage);
      }
    }
  }
}

function _havan_sso_session_destroy($account) {
  global $user;
  $session_destroy = array();
  $client_ip = get_ip_address();
  $sessions = _havan_sso_check_user_exist($account->uid, NULL, TRUE);
  foreach ($sessions as $session) {
    $session_destroy[] = $session->havan_sid;
  }
  if (count($session_destroy)) {
    $num_deleted = db_delete('havan_login_sessions')
        ->condition('havan_sid', $session_destroy, 'IN')
        ->execute();
    $num_deleted = db_delete('sessions')
        ->condition('sid', $session_destroy, 'IN')
        ->execute();
    $_SESSION = array();
    $user = drupal_anonymous_user();
    return TRUE;
  }
}
